--Inserting into Tables
-------------------------------------------------
INSERT INTO RankBase (RankID, RankName) VALUES 
(1, 'ChiefCooker'),
(2, 'ViceChiefCooker'),
(3, 'MainCooker'),
(4, 'Cooker'),
(5, 'JuniorCooker'),
(6, 'Janitor')

INSERT INTO EmployeeBase (EmployeeID, RankID, FirstName, SecondName, PassportID, MiddleName) VALUES 
(1, 1, 'Ivan', 'Kupriyanov', 8919433362, 'Prtrovich'),
(2, 2, 'Anna', 'Kupriyanova', 8919433364, NULL),
(3, 3, 'Petr', 'Zamok', 8919433365, 'Martynov'),
(4, 3, 'Dodo', 'Pashidze', 8924455361, NULL),
(5, 3, 'Kira', 'Emelyanova', 8900423365, NULL),
(6, 4, 'Vladimir', 'Moroz', 8009493325, 'Vladimirovich'),
(7, 4, 'Vladislav', 'Solnechny', 8229431345, 'Vladislavovich'),
(8, 4, 'Anton', 'Kovalsky', 7777712345, 'Aleksandrovich'),
(9, 5, 'Alexander', 'Orehov', 6767633365, NULL),
(10, 4, 'Egor', 'Pushkin', 8919411165, NULL),
(11, 5, 'Dmitry', 'Lermontov', 8910000005, NULL),
(12, 6, 'Alexei', 'Tolstoy', 8919764365, NULL)

DELETE from RankBase
DELETE from EmployeeBase
------------------------------------------------------------------------------------------------------
CREATE PROCEDURE Show_all_Employees
AS
select * from EmployeeBase
GO


CREATE PROCEDURE Find_Employee_by_ID
	@ident float
AS
	SET NOCOUNT ON
	select * from EmployeeBase where EmployeeID = @ident
GO

CREATE PROCEDURE Find_out_Rank_by_ID
	@ident float,
	@rankName varchar(max) output
AS
	SET NOCOUNT ON;
	SET @rankName = (SELECT RankBase.RankName
	FROM EmployeeBase
	LEFT JOIN RankBase
	ON EmployeeBase.RankID = RankBase.RankID WHERE EmployeeBase.EmployeeID = @ident);
GO

CREATE PROCEDURE Delete_Employee_by_ID
	@ident float
AS
	BEGIN
		DECLARE @id float, @cursor cursor;
		SET @cursor = CURSOR SCROLL FOR SELECT EmployeeID FROM EmployeeBase WHERE EmployeeID = @ident
		OPEN @cursor
		FETCH NEXT FROM @cursor INTO @id
		WHILE (@@FETCH_STATUS = 0)
		BEGIN
			 DELETE FROM EmployeeBase WHERE EmployeeID = @id
			 FETCH NEXT FROM @cursor INTO @id
		END
		CLOSE @cursor
		DEALLOCATE @cursor
	END
GO


exec Show_all_Employees 

exec Find_Employee_by_ID 2

Declare @test varchar(max);
exec Find_out_Rank_by_ID 3, @rankName = @test output;
print @test

exec Delete_Employee_by_ID 4
