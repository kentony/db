CREATE TRIGGER TRGR_T1
ON CustomerBase AFTER INSERT AS
BEGIN
	DECLARE @Telephone_VAR_D float
	DECLARE @Telephone_VAR_C varchar(MAX)
	DECLARE @Telephone_VAR_R varchar(11)
	SELECT @Telephone_VAR_D = (SELECT Telephone FROM inserted)
	SET @Telephone_VAR_C = (cast(replace(convert(decimal(15,2),@Telephone_VAR_D), '.', ',') as varchar(20)))
	SET @Telephone_VAR_R = SUBSTRING(@Telephone_VAR_C, 1, 11)

	INSERT INTO BaseTest(Telephone) VALUES (@Telephone_VAR_R)
END
GO

SELECT * From BaseTest
Delete from CustomerBase
Delete from BaseTest

Select * from CustomerBase
Select * from BaseTest

INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MiddleName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (1, 'Anton', 'Titov', 'Sergeyevich', 1000, 5, 89173038198);
INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MiddleName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (2, 'Ksenia', 'Moloko', 'Petrovna', 200, 1, 89156455464);
INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MiddleName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (3, 'Aidar', 'Matulin', 'Ahkmetov', 200, 1, 89275547973);
INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MiddleName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (4, 'Kirill', 'Kirilin', 'Kirillovich', 1000, 5, 89194531061);
INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MiddleName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (5, 'Maksim', 'Pogodin', 'Pavlovich', 1000, 5, 89194333062);
--One with NULL MiddleName--
INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (6, 'Ivan', 'Kupriyanov', 2000, 10, 89194333062);



-----------------------------------------------------------------------------------------------------------------
--DELETE ONE STRING (WHITH "WHERE" is there're not single string)
-----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER TRGR_T2
ON CustomerBase INSTEAD OF DELETE AS
BEGIN  
	IF @@ROWCOUNT = 0 return;

	DECLARE @CustomerID_VAR float	
	DECLARE @Telephone_VAR float
	DECLARE @Telephone_VAR_R varchar(11)

	SET @CustomerID_VAR = (SELECT CustomerID FROM deleted)
	SET @Telephone_VAR = (SELECT Telephone FROM deleted)
	SET @Telephone_VAR_R = (SUBSTRING(cast(replace(convert(decimal(15,2),@Telephone_VAR), '.', ',') as varchar(20)), 1, 11))

	DELETE FROM CustomerBase WHERE CustomerID = @CustomerID_VAR
	DELETE FROM BaseTest WHERE Telephone = @Telephone_VAR_R	
END
GO


------------------------------------------------------------------------------------------------------------------
--Delete *
------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER TRGR_T4
ON CustomerBase INSTEAD OF DELETE AS
BEGIN  
	IF @@ROWCOUNT = 0 return;
	DELETE FROM CustomerBase WHERE CustomerID in (SELECT CustomerID FROM deleted)
	DELETE FROM BaseTest WHERE Telephone in (SELECT (SUBSTRING(cast(replace(convert(decimal(15,2),Telephone), '.', ',') as varchar(20)), 1, 11)) FROM deleted)
END
GO


------------------------------------------------------------------------------------------------------------------
--UPDATE
------------------------------------------------------------------------------------------------------------------
DROP TRIGGER TRGR_T1
DROP TRIGGER TRGR_T2
DROP TRIGGER TRGR_T3
DROP TRIGGER TRGR_T4
Delete from CustomerBase
Delete from BaseTest
DROP TABLE BaseTest
CREATE TABLE BaseTest (
	Telephone varchar(11) NULL,
	ID float NOT NULL
)
GO

CREATE TRIGGER TRGR_T3
ON CustomerBase FOR UPDATE AS
BEGIN  
	IF @@ROWCOUNT = 0 return;

	DECLARE @CustomerID_VAR float
	DECLARE @Telephone_VAR float
	DECLARE @Telephone_VAR_R varchar(11)

	SET @CustomerID_VAR = (SELECT CustomerID FROM inserted)
	SET @Telephone_VAR = (SELECT Telephone FROM inserted)
	SET @Telephone_VAR_R = (SUBSTRING(cast(replace(convert(decimal(15,2),@Telephone_VAR), '.', ',') as varchar(20)), 1, 11))

	UPDATE BaseTest SET Telephone = @Telephone_VAR_R
	WHERE ID = @CustomerID_VAR
END
GO

CREATE TRIGGER TRGR_T1
ON CustomerBase AFTER INSERT AS
BEGIN
	DECLARE @CustomerID_VAR float	
	DECLARE @Telephone_VAR float
	DECLARE @Telephone_VAR_R varchar(11)

	SET @Telephone_VAR = (SELECT Telephone FROM inserted)
	SET @CustomerID_VAR = (SELECT CustomerID FROM inserted)
	SET @Telephone_VAR_R = SUBSTRING(cast(replace(convert(decimal(15,2),@Telephone_VAR), '.', ',') as varchar(20)), 1, 11)

	INSERT INTO BaseTest(Telephone, ID) VALUES (@Telephone_VAR_R, @CustomerID_VAR)
END
GO


UPDATE CustomerBase SET Telephone = 89276217575 WHERE CustomerID = 1
SELECT * From BaseTest

DROP TRIGGER TRGR_T1
DROP TRIGGER TRGR_T2
DROP TRIGGER TRGR_T3
Delete from CustomerBase
Delete from BaseTest
DROP TABLE BaseTest
