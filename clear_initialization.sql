CREATE TABLE CustomerBase(
	CustomerID float NOT NULL,
	FirstName varchar(20) NOT NULL,
	SecondName varchar(20) NOT NULL,
	MiddleName varchar(20) NULL,
	MoneySpendOveral float NOT NULL,
	PersonalDiscount float NOT NULL,
	Telephone float NOT NULL
	PRIMARY KEY (CustomerID)
)
GO

CREATE TABLE Menu(
	DishID float NOT NULL,
	DishName varchar(40) NOT NULL,
	DishCost float NOT NULL,
	DishType int NOT NULL,
	CurrentTime timestamp NOT NULL
	PRIMARY KEY (DishID)
)
GO

CREATE TABLE OrderContent(
	OrderID float NOT NULL,
	DishID float NOT NULL,
	CostPerPosition float NOT NULL,
)
GO

CREATE TABLE CurrentOrder(
	ChiefCooker float,
	CustomerID float NOT NULL,
	OrderID float NOT NULL,
	OrderTime timestamp,
	Summary float,
	PRIMARY KEY (OrderID)
)
GO

CREATE TABLE OrderArchive(
	ChiefCooker float,
	CustomerID float NOT NULL,
	OrderID float NOT NULL,
	OrderTime timestamp,
	Summary float,
	PRIMARY KEY (OrderID)
)
GO


CREATE TABLE CookingBase(
	ProductID float NOT NULL,
	Amount float NOT NULL,
	DishID float NOT NULL,
	CostPerPosition float NOT NULL
) 
GO

CREATE TABLE ProductBase(
	ProductID float NOT NULL,
	ProductName varchar(MAX) NOT NULL,
	ProductAmount float NOT NULL
) 
GO

CREATE TABLE Delivery(
	ProviderID float NOT NULL,
	ProductID float NOT NULL,
	ProductCost float NOT NULL,
	ProductCostInTimeOf time NOT NULL
)
GO

CREATE TABLE ProviderBase(
	ProviderID float NOT NULL,
	ProviderName varchar(MAX) NOT NULL
)
GO

CREATE TABLE RankBase(
	RankID float NOT NULL,
	RankName varchar(30) NOT NULL
)
GO

CREATE TABLE DurationOnDuty(
	EmployeeID float NOT NULL,
	TimeOnDuty time
)
GO

CREATE TABLE EmployeeBase(
	RankID float NOT NULL,
	EmployeeID float NOT NULL,
	FirstName varchar(MAX) NOT NULL,
	SecondName varchar(MAX) NOT NULL,
	MiddleName varchar(MAX) NULL,
	PassportID float NOT NULL,
	StartWorkingIn timestamp,
	TimeOnDuty time
	PRIMARY KEY (EmployeeID)
)
GO

CREATE TABLE dbo.BaseTest(
	Telephone varchar(max) NULL,
	MobileProvider varchar(4) NULL
)
GO
