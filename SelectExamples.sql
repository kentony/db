INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MiddleName, MoneySpendOveral, PersonalDiscount, Telephone) VALUES 
(1, 'Anton', 'Titov', 'Sergeyevich', 1000, 5, 89173038198),
(2, 'Ksenia', 'Moloko', 'Petrovna', 200, 1, 89156455464),
(3, 'Aidar', 'Matulin', 'Ahkmetov', 200, 1, 89275547973),
(4, 'Kirill', 'Kirilin', 'Kirillovich', 1000, 5, 89194531061),
(5, 'Maksim', 'Pogodin', 'Pavlovich', 1000, 5, 89194333062),
--One with NULL MiddleName--
INSERT INTO CustomerBase (CustomerID, FirstName, SecondName, MoneySpendOveral, PersonalDiscount, Telephone)
VALUES (6, 'Ivan', 'Kupriyanov', 2000, 10, 89194333062);

SELECT * FROM CustomerBase

INSERT INTO ProviderBase (ProviderID, ProviderName) VALUES
(1, 'HelloWorld'),
(2, 'ByeWorld'),
(3, 'MyWorld'),
(4, 'BestWorld'),
(5, 'BestWorld'),
(6, 'BestWorld'),
(7, 'ByeWorld')

INSERT INTO Delivery (ProviderID, ProductID, ProductCost, ProductCostInTimeOf)
VALUES(1, 1, 270, getdate());
INSERT INTO Delivery (ProviderID, ProductID, ProductCost, ProductCostInTimeOf)
VALUES(1, 2, 200, getdate());
INSERT INTO Delivery (ProviderID, ProductID, ProductCost, ProductCostInTimeOf)
VALUES(2, 3, 160, getdate());
INSERT INTO Delivery (ProviderID, ProductID, ProductCost, ProductCostInTimeOf)
VALUES(1, 4, 1200, getdate());
INSERT INTO Delivery (ProviderID, ProductID, ProductCost, ProductCostInTimeOf)
VALUES(2, 5, 850, getdate());
INSERT INTO Delivery (ProviderID, ProductID, ProductCost, ProductCostInTimeOf)
VALUES(3, 6, 250, getdate());

Select * from CustomerBase;

Select * from CustomerBase
WHERE MiddleName LIKE '%vi_h';

Select * from CustomerBase
WHERE FirstName NOT LIKE '[!AM]%';

Select * from CustomerBase
WHERE MoneySpendOveral IN (2000, 200);

Select * from CustomerBase
WHERE Telephone BETWEEN 89170000000 AND 89200000000;

Select Delivery.ProductCost, ProviderBase.ProviderName
From ProviderBase
INNER JOIN Delivery
ON ProviderBase.ProviderID=Delivery.ProviderID;

SELECT Delivery.ProductCost, ProviderBase.ProviderName
FROM ProviderBase
LEFT JOIN Delivery
ON ProviderBase.ProviderID=Delivery.ProviderID;

SELECT Delivery.ProductCost, ProviderBase.ProviderName
FROM ProviderBase
RIGHT OUTER JOIN Delivery
ON ProviderBase.ProviderID=Delivery.ProviderID;


SELECT ProviderName, COUNT(ProviderName) AS NubmerOfProviders
FROM ProviderBase
GROUP BY ProviderName; 


SELECT ISNULL(MiddleName, 'Smith') FROM CustomerBase;

Delete from CustomerBase;
Delete from ProviderBase;
Delete from Delivery;
