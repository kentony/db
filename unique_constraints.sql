ALTER TABLE CustomerBase
ADD CONSTRAINT RestDB_CustomerBase_CustomerID_Unique UNIQUE (CustomerID)
GO

ALTER TABLE Menu
ADD CONSTRAINT RestDB_Menu_DishID_Unique UNIQUE (DishID)
GO

ALTER TABLE ProductBase
ADD CONSTRAINT RestDB_ProductBase_ProductID_Unique UNIQUE (ProductID)
GO

ALTER TABLE ProviderBase
ADD CONSTRAINT RestDB_ProviderBase_ProviderID_Unique UNIQUE (ProviderID)
GO

ALTER TABLE RankBase
ADD CONSTRAINT RestDB_RankBase_RankID_Unique UNIQUE (RankID)
GO

ALTER TABLE RankBase
ADD CONSTRAINT RestDB_RankBase_RankName_Unique UNIQUE (RankName)
GO

ALTER TABLE DurationOnDuty
ADD CONSTRAINT RestDB_DurationOnDuty_EmployeeID_Unique UNIQUE (EmployeeID)
GO

ALTER TABLE EmployeeBase
ADD CONSTRAINT RestDB_EmployeeBase_PassportID_Unique UNIQUE (PassportID)
GO