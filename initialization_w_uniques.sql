CREATE TABLE CustomerBase(
	CustomerID float NOT NULL UNIQUE,
	FirstName varchar(20) NOT NULL,
	SecondName varchar(20) NOT NULL,
	MiddleName varchar(20) NULL,
	MoneySpendOveral float NOT NULL,
	PersonalDiscount float NOT NULL,
	Telephone float NOT NULL
	PRIMARY KEY (CustomerID)
)
GO

CREATE TABLE Menu(
	DishID float NOT NULL UNIQUE,
	DishName varchar(20) NOT NULL,
	DishCost float NOT NULL,
	DishType int NOT NULL,
	CurrentTime timestamp NOT NULL
	PRIMARY KEY (DishID)
)
GO

CREATE TABLE OrderContent(
	OrderID float NOT NULL,
	DishID float NOT NULL,
	CostPerPosition float NOT NULL,
)
GO

CREATE TABLE CurrentOrder(
	ChiefCooker float,
	CustomerID float NOT NULL,
	OrderID float NOT NULL,
	OrderTime timestamp,
	Summary float,
	PRIMARY KEY (OrderID)
)
GO

CREATE TABLE OrderArchive(
	ChiefCooker float,
	CustomerID float NOT NULL,
	OrderID float NOT NULL,
	OrderTime timestamp,
	Summary float,
	PRIMARY KEY (OrderID)
)
GO


CREATE TABLE CookingBase(
	ProductID float NOT NULL,
	Amount float NOT NULL,
	DishID float NOT NULL,
	CostPerPosition float NOT NULL
) 
GO

CREATE TABLE ProductBase(
	ProductID float NOT NULL UNIQUE,
	ProductName varchar NOT NULL,
	ProductAmount float NOT NULL
) 
GO

CREATE TABLE Delivery(
	ProviderID float NOT NULL,
	ProductID float NOT NULL,
	ProductCost float NOT NULL,
	ProductCostInTimeOf time NOT NULL
)
GO

CREATE TABLE ProviderBase(
	ProviderID float NOT NULL UNIQUE,
	ProviderName varchar NOT NULL
)
GO

CREATE TABLE RankBase(
	RankID float NOT NULL UNIQUE,
	RankName varchar NOT NULL UNIQUE
)
GO

CREATE TABLE DurationOnDuty(
	EmployeeID float NOT NULL UNIQUE,
	TimeOnDuty time
)
GO

CREATE TABLE EmployeeBase(
	RankID float NOT NULL,
	EmployeeID float NOT NULL,
	FirstName varchar NOT NULL,
	SecondName varchar NOT NULL,
	MiddleName varchar NULL,
	PassportID float NOT NULL UNIQUE,
	StartWorkingIn timestamp,
	TimeOnDuty time
)
GO
