--Inserting into Tables
-------------------------------------------------
INSERT INTO RankBase (RankID, RankName) VALUES 
(1, 'ChiefCooker'),
(2, 'ViceChiefCooker'),
(3, 'MainCooker'),
(4, 'Cooker'),
(5, 'JuniorCooker'),
(6, 'Janitor')

INSERT INTO EmployeeBase (EmployeeID, RankID, FirstName, SecondName, PassportID, MiddleName) VALUES 
(1, 1, 'Ivan', 'Kupriyanov', 8919433362, 'Prtrovich'),
(2, 2, 'Anna', 'Kupriyanova', 8919433364, NULL),
(3, 3, 'Petr', 'Zamok', 8919433365, 'Martynov'),
(4, 3, 'Dodo', 'Pashidze', 8924455361, NULL),
(5, 3, 'Kira', 'Emelyanova', 8900423365, NULL),
(6, 4, 'Vladimir', 'Moroz', 8009493325, 'Vladimirovich'),
(7, 4, 'Vladislav', 'Solnechny', 8229431345, 'Vladislavovich'),
(8, 4, 'Anton', 'Kovalsky', 7777712345, 'Aleksandrovich'),
(9, 5, 'Alexander', 'Orehov', 6767633365, NULL),
(10, 4, 'Egor', 'Pushkin', 8919411165, NULL),
(11, 5, 'Dmitry', 'Lermontov', 8910000005, NULL),
(12, 6, 'Alexei', 'Tolstoy', 8919764365, NULL)

DELETE from RankBase
DELETE from EmployeeBase
-------------------------------------------------
--Views
-------------------------------------------------

--1
-------------------------------------------------
CREATE VIEW View_1 AS
SELECT EmployeeBase.EmployeeID, RankBase.RankName
FROM EmployeeBase
LEFT JOIN RankBase
ON EmployeeBase.RankID = RankBase.RankID;
GO


Select * from View_1
sp_helptext View_1

DROP VIEW View_1

CREATE VIEW View_1 
WITH ENCRYPTION AS
SELECT EmployeeBase.EmployeeID, RankBase.RankName
FROM EmployeeBase
LEFT JOIN RankBase
ON EmployeeBase.RankID = RankBase.RankID;
GO

Select * from View_1
sp_helptext View_1

-------------------------------------------------
---2
-------------------------------------------------

--CREATE VIEW View_Only_Senior_Employees 
--WITH ENCRYPTION AS
--SELECT EmployeeBase.EmployeeID, RankBase.RankName, EmployeeBase.RankID, EmployeeBase.FirstName, EmployeeBase.SecondName, EmployeeBase.PassportID 
--from EmployeeBase
--LEFT JOIN RankBase
--ON EmployeeBase.RankID = RankBase.RankID
--Where EmployeeBase.RankID <=3
--WITH CHECK OPTION;
--GO

CREATE VIEW View_Only_Employees_Without_MiddleName 
WITH ENCRYPTION AS
SELECT EmployeeBase.EmployeeID, RankBase.RankName, EmployeeBase.RankID, EmployeeBase.FirstName, EmployeeBase.MiddleName, EmployeeBase.SecondName, EmployeeBase.PassportID 
from EmployeeBase
LEFT JOIN RankBase
ON EmployeeBase.RankID = RankBase.RankID
Where EmployeeBase.MiddleName IS NULL
WITH CHECK OPTION;
GO


INSERT INTO View_Only_Employees_Without_MiddleName (EmployeeID, RankID, FirstName, SecondName, PassportID, MiddleName) VALUES 
(13, 1, 'Ivan', 'Kupriyanov', 8919414362, 'Prtrovich')
INSERT INTO View_Only_Employees_Without_MiddleName (EmployeeID, RankID, FirstName, SecondName, PassportID, MiddleName) VALUES 
(13, 2, 'Anna', 'Kupriyanova', 8919153364, NULL)



Select * from View_Only_Employees_Without_MiddleName 
Select * from EmployeeBase
DROP VIEW View_Only_Employees_Without_MiddleName 

Select * from View_Only_Senior_Employees
DROP VIEW View_Only_Senior_Employees

-------------------------------------------------
---3
-------------------------------------------------

SET NUMERIC_ROUNDABORT OFF;
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL,
ARITHABORT, QUOTED_IDENTIFIER, ANSI.NULLS ON;
GO

--CREATE VIEW View_Only_Senior_Employees_A
--With SCHEMABINDING
--As
--SELECT EmployeeID, RankID, FirstName, SecondName, PassportID
--FROM DBO.EmployeeBase
--GO

CREATE VIEW View_Only_Senior_Employees_B
With SCHEMABINDING
As
SELECT EmployeeID, RankID, FirstName, SecondName, PassportID
FROM DBO.EmployeeBase
GO

CREATE UNIQUE CLUSTERED INDEX indClustered
ON View_Only_Senior_Employees_B (EmployeeID)
GO

--Select * from View_Only_Senior_Employees_A
Select * from View_Only_Senior_Employees_B WITH (NOEXPAND)
Where RankID = 3
Order by EmployeeID ASC;
GO

DROP INDEX indClustered
--DROP VIEW View_Only_Senior_Employees_A
DROP VIEW View_Only_Senior_Employees_B
