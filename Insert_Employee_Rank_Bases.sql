INSERT INTO RankBase (RankID, RankName) VALUES 
(1, 'ChiefCooker'),
(2, 'ViceChiefCooker'),
(3, 'MainCooker'),
(4, 'Cooker'),
(5, 'JuniorCooker'),
(6, 'Janitor')

INSERT INTO EmployeeBase (EmployeeID, RankID, FirstName, SecondName, PassportID) VALUES 
(1, 1, 'Ivan', 'Kupriyanov', 8919433362),
(2, 2, 'Anna', 'Kupriyanova', 8919433364),
(3, 3, 'Petr', 'Zamok', 8919433365),
(4, 3, 'Dodo', 'Pashidze', 8924455361),
(5, 3, 'Kira', 'Emelyanova', 8900423365),
(6, 4, 'Vladimir', 'Moroz', 8009493325),
(7, 4, 'Vladislav', 'Solnechny', 8229431345),
(8, 4, 'Anton', 'Kovalsky', 7777712345),
(9, 5, 'Alexander', 'Orehov', 6767633365),
(10, 4, 'Egor', 'Pushkin', 8919411165),
(11, 5, 'Dmitry', 'Lermontov', 8910000005),
(12, 6, 'Alexei', 'Tolstoy', 8919764365)


--SELECT EmployeeBase.EmployeeID, RankBase.RankName
--FROM EmployeeBase
--LEFT JOIN RankBase
--ON EmployeeBase.RankID = RankBase.RankID;
